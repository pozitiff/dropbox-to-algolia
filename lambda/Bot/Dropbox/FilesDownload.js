


var DropboxAbstract = require('./Abstract');
DropboxFilesListFolder.prototype = new DropboxAbstract();

function DropboxFilesListFolder(pathData)
{
    var self = this;
    this.initFileStream = function(cbStream) {

        var parameters = {};
        self.callAPI({
            resource: 'files/download',
            parameters: {
                path: pathData.id || pathData.path || pathData.rev
            }
        }, (result, response) => {
        }).pipe(cbStream);
    }
}

module.exports = DropboxFilesListFolder;

