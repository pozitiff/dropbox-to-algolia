
var Abstract = require('./Abstract');
DropboxAlgoliaBoth.prototype = new Abstract();

function DropboxAlgoliaBoth(isLamdba)
{
    var self = this;
    this.isLamdba = isLamdba || false;
    var CacheService = 'algolia' === this.getConfig().cache.type.toLowerCase()
            ? require('./Services/Cache/Algolia')
            : require('./Services/Cache/File');
    var AlgoliaService = require('./Services/Algolia');
    var HttpService = require('./Services/Http');
    var QueueService = require('./Services/Queue');

    var services = {
        http: new HttpService(),
        queue: new QueueService(),
        cache: new CacheService(),
        algolia: new AlgoliaService()
    };

    var parsers = {
        doc:require('./Parsers/Doc'),
        docx:require('./Parsers/Docx'),
        pdf:require('./Parsers/Pdf')
    };

    var dropboxes = {
        SharingGetFileMetadata: require('./Dropbox/SharingGetFileMetadata'),
        DropboxFilesListFolder: require('./Dropbox/FilesListFolder'),
        FilesDownload: require('./Dropbox/FilesDownload')
    };
    var DropboxFilesListFolder = new dropboxes.DropboxFilesListFolder();

    services.queue.setField('dropboxes', dropboxes).setField('parsers', parsers).setField('services', services);

    this.on('file', function(fileData, cb){
        services.cache.check(fileData.id, fileData.content_hash, function(cacheRes){
            if (!cacheRes) {
                self.runNewFile(fileData, cb);
            } else {
                cb();
            }
        });
    });

    this.on('dropbox-changed', function(){
        DropboxFilesListFolder.prepareFiles();
    });

    this.on('dropbox-removed-file', function(dbxId, cb){
        services.algolia.rm(dbxId, function(){
            console.log('Removed from Algolia', dbxId);
            cb();
        });
    });

    this.on('start-check-file-list', function(){
        services.cache.startWatching();
    });

    this.on('finish-check-file-list', function(){

        services.cache.endWatching();
    });

    this.runNewFile = function(fileData, cb) {
        services.queue.add(fileData);
        cb();
    };

    this.addToQueue = function(fileData, cb) {
        services.queue.add(fileData, cb).run();
    };

    if (!self.isLamdba) {
        DropboxFilesListFolder.prepareFiles();
        services.queue.run();
    }
}

module.exports = DropboxAlgoliaBoth;

