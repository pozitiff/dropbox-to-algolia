
var EventEmitter = require('events');
Abstract.prototype = new EventEmitter();
function Abstract() { }


var _config = null;

Abstract.prototype.getConfig = function(){
    if (!_config) {
        _config = require('./config');
    }
    return _config;
};

Abstract.prototype.setField = function(f, v){
    this[f] = v;
    return this;
};

module.exports = Abstract;