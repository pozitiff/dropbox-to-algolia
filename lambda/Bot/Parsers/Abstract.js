
var Abstract = require('../Abstract');
var fs = require('fs');

ParserAbstract.prototype = new Abstract();

function ParserAbstract() { }

ParserAbstract.prototype.tmpFilePath = '';
ParserAbstract.prototype.stream = false;
ParserAbstract.prototype.getStream = function(path) {
    var self = this;
    console.log('Downloading', path)
    this.tmpFilePath = this.getConfig().dropbox.tmpFolder + path.replace(/\//g, '-');
    this.stream = fs.createWriteStream(this.tmpFilePath);

    this.stream.on('finish', function () {
        self.emit('stream-finish', self.tmpFilePath);
        self.onStreamFinish();
    });
    return this.stream;
};

ParserAbstract.prototype.onStreamFinish = function(){};
ParserAbstract.prototype.setCb = function(_cb){
    if ('function' == typeof _cb) {
        this.cb = _cb;
    }
};
ParserAbstract.prototype.cb = function(){};
ParserAbstract.prototype.onReay = function(content){
    var self = this;
    fs.exists(self.tmpFilePath, function(exists){
        if (exists) {
            fs.unlinkSync(self.tmpFilePath);
        }
    });
    this.cb(content);
};

module.exports = ParserAbstract;

